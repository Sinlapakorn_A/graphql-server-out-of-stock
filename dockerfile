# Set the base image to node:15-alpine
FROM node:15-alpine as build

# Specify where our app will live in the container
WORKDIR /app

# Copy the Server to the container
COPY . /app/

# Prepare the container for building Server
RUN npm install

# Run server
CMD [ "node", "index.js" ]
