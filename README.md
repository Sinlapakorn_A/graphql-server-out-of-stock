<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Demo
  [http://139.5.144.28:4000/](http://139.5.144.28:4000/)

### Installation

1. Clone the repo
  ```sh
  git clone https://gitlab.com/Sinlapakorn_A/graphql-server-out-of-stock.git
  ```
2. Install NPM packages
  ```sh
  npm install
  ```

### Run

1. Start this application
  ```sh
  node index.js
  ```
2. Then open [http://localhost:4000/](http://localhost:4000/) to see your app.