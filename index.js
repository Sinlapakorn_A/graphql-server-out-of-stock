const { ApolloServer, gql } = require('apollo-server');
// const { sneaker } = require('./constant/sneaker');
// import sneaker from "./constant/sneaker";

// A schema is a collection of type definitions (hence "typeDefs")
const typeDefs = gql`
  # This "Sneaker" type defines the queryable fields for every sneaker in our data source.
  type Sneaker {
    id: String,
    brand: String,
    colorway: String,
    gender: String,
    name: String,
    releaseDate: String,
    retailPrice: Int,
    shoe: String,
    styleId: String,
    title: String,
    year: Int,
    media: Media
  }

  type Media {
    imageUrl: String,
    smallImageUrl: String,
    thumbUrl: String
  }

  # The "Query" type is special: it lists all of the available queries that
  type Query {
    sneaker(search: String): [Sneaker]
  }
`;

// schema. This resolver retrieves sneaker from the "sneaker" ****from "./constant/sneaker".
const resolvers = {
  Query: {
    sneaker: (obj, args, context, info) => sneaker.filter(function (item) {
      if (item.brand.toLowerCase().includes(args.search.toLowerCase()) || item.name.toLowerCase().includes(args.search.toLowerCase()) || item.title.toLowerCase().includes(args.search.toLowerCase())) {
        return true;
      } else {
        return false;
      }
    })
  },
};

// The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers.
const server = new ApolloServer({ typeDefs, resolvers });

// The `listen` method launches a web server.
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});

const sneaker = [
  {
    "id": "c46b2fd1-d7ec-43d8-a715-c0c4a6dbdfde",
    "brand": "Nike",
    "colorway": "Black/University Red/White",
    "gender": "child",
    "name": "Black University Red (GS)",
    "releaseDate": "2021-07-16",
    "retailPrice": 65,
    "shoe": "Court Borough Low 2",
    "styleId": "BQ5448-007",
    "title": "Court Borough Low 2 Black University Red (GS)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Court-Borough-Low-2-Black-University-Red-GS.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614886339",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Court-Borough-Low-2-Black-University-Red-GS.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614886339",
      "thumbUrl": "https://images.stockx.com/images/Nike-Court-Borough-Low-2-Black-University-Red-GS.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614886339"
    }
  },
  {
    "id": "333ea741-ee8b-4d4c-b278-48b673d747e8",
    "brand": "Nike",
    "colorway": "White/Game Royal",
    "gender": "men",
    "name": "Game Royal",
    "releaseDate": "2021-06-11",
    "retailPrice": 120,
    "shoe": "Nike Dunk High",
    "styleId": "DD1399-102",
    "title": "Nike Dunk High Game Royal",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Dunk-High-Game-Royal.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1613038501",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Dunk-High-Game-Royal.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1613038501",
      "thumbUrl": "https://images.stockx.com/images/Nike-Dunk-High-Game-Royal.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1613038501"
    }
  },
  {
    "id": "348952df-d518-4987-bd0f-e7fd5a6b2821",
    "brand": "Jordan",
    "colorway": "White/Tech Grey/Black/Fire Red",
    "gender": "child",
    "name": "White Oreo (2021) (GS)",
    "releaseDate": "2021-05-29",
    "retailPrice": 190,
    "shoe": "Jordan 4 Retro",
    "styleId": "",
    "title": "Jordan 4 Retro White Oreo (2021) (GS)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Air-Jordan-4-Retro-White-Oreo-2021-GS.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614141824",
      "smallImageUrl": "https://images.stockx.com/images/Air-Jordan-4-Retro-White-Oreo-2021-GS.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614141824",
      "thumbUrl": "https://images.stockx.com/images/Air-Jordan-4-Retro-White-Oreo-2021-GS.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614141824"
    }
  },
  {
    "id": "b48c489c-6bf7-49a9-87be-25fa1117a0b5",
    "brand": "Jordan",
    "colorway": "White/Tech Grey/Black/Fire Red",
    "gender": "men",
    "name": "White Oreo (2021)",
    "releaseDate": "2021-05-29",
    "retailPrice": 190,
    "shoe": "Jordan 4 Retro",
    "styleId": "CT8527-100",
    "title": "Jordan 4 Retro White Oreo (2021)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Air-Jordan-4-Retro-White-Oreo-2021.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614719033",
      "smallImageUrl": "https://images.stockx.com/images/Air-Jordan-4-Retro-White-Oreo-2021.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614719033",
      "thumbUrl": "https://images.stockx.com/images/Air-Jordan-4-Retro-White-Oreo-2021.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614719033"
    }
  },
  {
    "id": "848fa9c8-f753-4f4a-a2b5-8bc336057012",
    "brand": "Jordan",
    "colorway": "White/Bright Citrus",
    "gender": "women",
    "name": "Citrus (W)",
    "releaseDate": "2021-05-06",
    "retailPrice": 185,
    "shoe": "Jordan 11 Retro Low",
    "styleId": "AH7860-139",
    "title": "Jordan 11 Retro Low Bright Citrus (W)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Air-Jordan-11-Retro-Low-Bright-Citrus-W.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612989043",
      "smallImageUrl": "https://images.stockx.com/images/Air-Jordan-11-Retro-Low-Bright-Citrus-W.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612989043",
      "thumbUrl": "https://images.stockx.com/images/Air-Jordan-11-Retro-Low-Bright-Citrus-W.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612989043"
    }
  },
  {
    "id": "adf1ea5a-4ce7-4d21-a789-3b675978ba26",
    "brand": "adidas",
    "colorway": "Footwear White/Footwear White",
    "gender": "men",
    "name": "Triple White",
    "releaseDate": "2021-05-01",
    "retailPrice": 0,
    "shoe": "adidas Forum Mid",
    "styleId": "FY4975",
    "title": "adidas Forum Mid Triple White",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-Forum-Mid-Triple-White.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614290906",
      "smallImageUrl": "https://images.stockx.com/images/adidas-Forum-Mid-Triple-White.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614290906",
      "thumbUrl": "https://images.stockx.com/images/adidas-Forum-Mid-Triple-White.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614290906"
    }
  },
  {
    "id": "d5069760-f955-404d-8f27-f484b22e07b4",
    "brand": "Jordan",
    "colorway": "White/White-Black-Legend Blue",
    "gender": "men",
    "name": "Legend Blue",
    "releaseDate": "2021-04-24",
    "retailPrice": 185,
    "shoe": "Jordan 11 Retro Low",
    "styleId": "AV2187-117",
    "title": "Jordan 11 Retro Low Legend Blue",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Air-Jordan-11-Retro-Low-Legend-Blue.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614727489",
      "smallImageUrl": "https://images.stockx.com/images/Air-Jordan-11-Retro-Low-Legend-Blue.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614727489",
      "thumbUrl": "https://images.stockx.com/images/Air-Jordan-11-Retro-Low-Legend-Blue.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614727489"
    }
  },
  {
    "id": "28aff902-d9a8-4dfa-b6b8-12d2971eefcb",
    "brand": "Jordan",
    "colorway": "Hyper Royal/Light Smoke Grey-White",
    "gender": "men",
    "name": "Hyper Royal Smoke Grey",
    "releaseDate": "2021-04-17",
    "retailPrice": 170,
    "shoe": "Jordan 1 Retro High",
    "styleId": "555088-402",
    "title": "Jordan 1 Retro High OG Hyper Royal",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Air-Jordan-1-Retro-High-Hyper-Royal-Smoke-Grey.png?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1610040536",
      "smallImageUrl": "https://images.stockx.com/images/Air-Jordan-1-Retro-High-Hyper-Royal-Smoke-Grey.png?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1610040536",
      "thumbUrl": "https://images.stockx.com/images/Air-Jordan-1-Retro-High-Hyper-Royal-Smoke-Grey.png?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1610040536"
    }
  },
  {
    "id": "a8c83992-1d91-4f46-a174-12e2785d999b",
    "brand": "Jordan",
    "colorway": "Sail/Rust Pink-White-Crimson",
    "gender": "women",
    "name": "Rust Pink (W)",
    "releaseDate": "2021-04-15",
    "retailPrice": 190,
    "shoe": "Jordan 3 Retro",
    "styleId": "CK9246-600",
    "title": "Jordan 3 Retro Rust Pink (W)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Air-Jordan-3-Retro-Rust-Pink-W.png?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1611134373",
      "smallImageUrl": "https://images.stockx.com/images/Air-Jordan-3-Retro-Rust-Pink-W.png?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1611134373",
      "thumbUrl": "https://images.stockx.com/images/Air-Jordan-3-Retro-Rust-Pink-W.png?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1611134373"
    }
  },
  {
    "id": "95d18fb5-f608-42fe-82b3-a2b8763a9fe4",
    "brand": "Jordan",
    "colorway": "Varsity Red/Black-White",
    "gender": "men",
    "name": "Raging Bulls Red (2021)",
    "releaseDate": "2021-04-10",
    "retailPrice": 190,
    "shoe": "Jordan 5 Retro",
    "styleId": "DD0587-600",
    "title": "Jordan 5 Retro Raging Bulls Red (2021)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Air-Jordan-5-Retro-Raging-Bulls-Red-2021.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612835247",
      "smallImageUrl": "https://images.stockx.com/images/Air-Jordan-5-Retro-Raging-Bulls-Red-2021.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612835247",
      "thumbUrl": "https://images.stockx.com/images/Air-Jordan-5-Retro-Raging-Bulls-Red-2021.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612835247"
    }
  },
  {
    "id": "b8c8398d-f0ee-47f8-8c47-75b70fac9190",
    "brand": "Jordan",
    "colorway": "Varsity Red/Black-White",
    "gender": "child",
    "name": "Raging Bulls Red 2021 (GS)",
    "releaseDate": "2021-04-10",
    "retailPrice": 140,
    "shoe": "Jordan 5 Retro",
    "styleId": "",
    "title": "Jordan 5 Retro Raging Bulls Red 2021 (GS)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Air-Jordan-5-Retro-Raging-Bulls-Red-2021-GS.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612835480",
      "smallImageUrl": "https://images.stockx.com/images/Air-Jordan-5-Retro-Raging-Bulls-Red-2021-GS.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612835480",
      "thumbUrl": "https://images.stockx.com/images/Air-Jordan-5-Retro-Raging-Bulls-Red-2021-GS.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612835480"
    }
  },
  {
    "id": "0dd567b0-9e86-405e-84ca-68bb50331339",
    "brand": "Jordan",
    "colorway": "White/White-Multi-Color",
    "gender": "men",
    "name": "Easter (2021)",
    "releaseDate": "2021-04-03",
    "retailPrice": 200,
    "shoe": "Jordan 12 Retro Low",
    "styleId": "DB0733-190",
    "title": "Jordan 12 Retro Low Easter (2021)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Air-Jordan-12-Retro-Low-Easter-2021.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612981422",
      "smallImageUrl": "https://images.stockx.com/images/Air-Jordan-12-Retro-Low-Easter-2021.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612981422",
      "thumbUrl": "https://images.stockx.com/images/Air-Jordan-12-Retro-Low-Easter-2021.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612981422"
    }
  },
  {
    "id": "3aeda8df-7293-477f-9413-0e518eed90e3",
    "brand": "Jordan",
    "colorway": "Black/White-Electric Green-Violet Shock-Green Glow",
    "gender": "child",
    "name": "Electric Green (GS)",
    "releaseDate": "2021-04-01",
    "retailPrice": 140,
    "shoe": "Jordan 3 Retro",
    "styleId": "DA2304-003",
    "title": "Jordan 3 Retro Electric Green (GS)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Air-Jordan-3-Retro-Electric-Green-GS.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568363",
      "smallImageUrl": "https://images.stockx.com/images/Air-Jordan-3-Retro-Electric-Green-GS.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568363",
      "thumbUrl": "https://images.stockx.com/images/Air-Jordan-3-Retro-Electric-Green-GS.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568363"
    }
  },
  {
    "id": "207be8a3-4b9c-404c-82d4-cbb7a555844c",
    "brand": "adidas",
    "colorway": "Sand/Sand/Sand",
    "gender": "men",
    "name": "Sand",
    "releaseDate": "2021-03-26",
    "retailPrice": 80,
    "shoe": "adidas Yeezy Foam RNNR",
    "styleId": "FY4567",
    "title": "adidas Yeezy Foam RNNR Sand",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-Yeezy-Foam-RNNR-Sand.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793279",
      "smallImageUrl": "https://images.stockx.com/images/adidas-Yeezy-Foam-RNNR-Sand.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793279",
      "thumbUrl": "https://images.stockx.com/images/adidas-Yeezy-Foam-RNNR-Sand.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793279"
    }
  },
  {
    "id": "73ca647a-c823-4042-b4f5-d31b7644f4b9",
    "brand": "adidas",
    "colorway": "Moon Gray/Moon Gray/Moon Gray",
    "gender": "men",
    "name": "MXT Moon Gray",
    "releaseDate": "2021-03-26",
    "retailPrice": 80,
    "shoe": "adidas Yeezy Foam RNNR",
    "styleId": "",
    "title": "adidas Yeezy Foam RNNR MXT Moon Gray",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-Yeezy-Foam-RNNR-MXT-Moon-Gray.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793277",
      "smallImageUrl": "https://images.stockx.com/images/adidas-Yeezy-Foam-RNNR-MXT-Moon-Gray.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793277",
      "thumbUrl": "https://images.stockx.com/images/adidas-Yeezy-Foam-RNNR-MXT-Moon-Gray.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793277"
    }
  },
  {
    "id": "b2330041-6173-41e6-a681-a72a8ac674e4",
    "brand": "Nike",
    "colorway": "Sail-Sheen-Straw-Medium Brown",
    "gender": "men",
    "name": "Bacon (2021)",
    "releaseDate": "2021-03-26",
    "retailPrice": 140,
    "shoe": "Nike Air Max 90 NRG",
    "styleId": "CU1816-100",
    "title": "Nike Air Max 90 NRG Bacon (2021)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Air-Max-90-NRG-Bacon-2021.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612863371",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Air-Max-90-NRG-Bacon-2021.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612863371",
      "thumbUrl": "https://images.stockx.com/images/Nike-Air-Max-90-NRG-Bacon-2021.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612863371"
    }
  },
  {
    "id": "06012913-5ee9-4269-9495-388f44f66cd4",
    "brand": "adidas",
    "colorway": "Blue/Core White/Core Black",
    "gender": "men",
    "name": "Craig Green Blue",
    "releaseDate": "2021-03-22",
    "retailPrice": 310,
    "shoe": "adidas ZX 2K Phormar",
    "styleId": "FY5717",
    "title": "adidas ZX 2K Phormar Craig Green Blue",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-ZX-2K-Phormar-Craig-Green-Blue.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793285",
      "smallImageUrl": "https://images.stockx.com/images/adidas-ZX-2K-Phormar-Craig-Green-Blue.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793285",
      "thumbUrl": "https://images.stockx.com/images/adidas-ZX-2K-Phormar-Craig-Green-Blue.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793285"
    }
  },
  {
    "id": "4a2e4708-b832-4535-b8ef-5da18b96cb15",
    "brand": "adidas",
    "colorway": "Core White/Core White/Core Black",
    "gender": "men",
    "name": "Craig Green White",
    "releaseDate": "2021-03-22",
    "retailPrice": 310,
    "shoe": "adidas ZX 2K Phormar",
    "styleId": "FY5719",
    "title": "adidas ZX 2K Phormar Craig Green White",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-ZX-2K-Phormar-Craig-Green-White.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793291",
      "smallImageUrl": "https://images.stockx.com/images/adidas-ZX-2K-Phormar-Craig-Green-White.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793291",
      "thumbUrl": "https://images.stockx.com/images/adidas-ZX-2K-Phormar-Craig-Green-White.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793291"
    }
  },
  {
    "id": "6668c710-acda-4e49-951f-4f45a651d710",
    "brand": "Jordan",
    "colorway": "White/Arctic Punch-Hyper Pink",
    "gender": "child",
    "name": "Arctic Punch (GS)",
    "releaseDate": "2021-03-22",
    "retailPrice": 140,
    "shoe": "Jordan 12 Retro",
    "styleId": "510815-101",
    "title": "Jordan 12 Retro Arctic Punch (GS)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Air-Jordan-12-Retro-Arctic-Punch-GS.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614461601",
      "smallImageUrl": "https://images.stockx.com/images/Air-Jordan-12-Retro-Arctic-Punch-GS.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614461601",
      "thumbUrl": "https://images.stockx.com/images/Air-Jordan-12-Retro-Arctic-Punch-GS.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614461601"
    }
  },
  {
    "id": "d62fe47c-223e-4e8a-af69-6c1633783495",
    "brand": "Jordan",
    "colorway": "Midnight Navy/Cement Grey-White",
    "gender": "men",
    "name": "Georgetown (2021)",
    "releaseDate": "2021-03-20",
    "retailPrice": 190,
    "shoe": "Jordan 3 Retro",
    "styleId": "CT8532-401",
    "title": "Jordan 3 Retro Georgetown (2021)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Air-Jordan-3-Retro-Georgetown-2021.png?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1611172835",
      "smallImageUrl": "https://images.stockx.com/images/Air-Jordan-3-Retro-Georgetown-2021.png?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1611172835",
      "thumbUrl": "https://images.stockx.com/images/Air-Jordan-3-Retro-Georgetown-2021.png?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1611172835"
    }
  },
  {
    "id": "035eeebb-58d5-4370-b9ab-3a948eff2bf2",
    "brand": "Asics",
    "colorway": "Bamboo/Bamboo/White",
    "gender": "men",
    "name": "Kengo Kuma Bamboo",
    "releaseDate": "2021-03-19",
    "retailPrice": 370,
    "shoe": "ASICS Metaride AMU",
    "styleId": "",
    "title": "ASICS Metaride AMU Kengo Kuma Bamboo",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/ASICS-Metaride-AMU-Kengo-Kuma-Bamboo.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1613788033",
      "smallImageUrl": "https://images.stockx.com/images/ASICS-Metaride-AMU-Kengo-Kuma-Bamboo.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1613788033",
      "thumbUrl": "https://images.stockx.com/images/ASICS-Metaride-AMU-Kengo-Kuma-Bamboo.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1613788033"
    }
  },
  {
    "id": "5d271e81-a6f4-4ba2-ad36-d3fe39d4b8f2",
    "brand": "Nike",
    "colorway": "Black/Light Blue-White",
    "gender": "men",
    "name": "Rice Ball",
    "releaseDate": "2021-03-19",
    "retailPrice": 140,
    "shoe": "Nike Air Max 90",
    "styleId": "DD5483-010",
    "title": "Nike Air Max 90 Rice Ball",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Air-Max-90-Rice-Ball.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614290863",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Air-Max-90-Rice-Ball.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614290863",
      "thumbUrl": "https://images.stockx.com/images/Nike-Air-Max-90-Rice-Ball.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614290863"
    }
  },
  {
    "id": "873fde62-b497-4839-ad62-6fdcbbda87c6",
    "brand": "Asics",
    "colorway": "Blue/Pink-Grey/Navy",
    "gender": "men",
    "name": "Back Streets of Japan Pink Blue",
    "releaseDate": "2021-03-19",
    "retailPrice": 0,
    "shoe": "ASICS Gel-Lyte III",
    "styleId": "",
    "title": "ASICS Gel-Lyte III Back Streets of Japan Pink Blue",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/ASICS-Gel-Lyte-III-Back-Streets-of-Japan-Pink-Blue.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615310408",
      "smallImageUrl": "https://images.stockx.com/images/ASICS-Gel-Lyte-III-Back-Streets-of-Japan-Pink-Blue.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615310408",
      "thumbUrl": "https://images.stockx.com/images/ASICS-Gel-Lyte-III-Back-Streets-of-Japan-Pink-Blue.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615310408"
    }
  },
  {
    "id": "b5895cdb-2f53-4351-bf21-15a58f16c274",
    "brand": "Asics",
    "colorway": "Grey/Grey-Green",
    "gender": "men",
    "name": "Back Streets of Japan Grey Green",
    "releaseDate": "2021-03-19",
    "retailPrice": 0,
    "shoe": "ASICS Gel-Lyte III",
    "styleId": "",
    "title": "ASICS Gel-Lyte III Back Streets of Japan Grey Green",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/ASICS-Gel-Lyte-III-Back-Streets-of-Japan-Grey-Green.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615310405",
      "smallImageUrl": "https://images.stockx.com/images/ASICS-Gel-Lyte-III-Back-Streets-of-Japan-Grey-Green.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615310405",
      "thumbUrl": "https://images.stockx.com/images/ASICS-Gel-Lyte-III-Back-Streets-of-Japan-Grey-Green.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615310405"
    }
  },
  {
    "id": "bbce7608-47bd-4399-83fc-663c104edea1",
    "brand": "Reebok",
    "colorway": "Black/Hypergreen-Rbk Red",
    "gender": "men",
    "name": "ZX Pump",
    "releaseDate": "2021-03-19",
    "retailPrice": 180,
    "shoe": "adidas ZX Fury",
    "styleId": "GZ7286",
    "title": "adidas ZX Fury ZX Pump",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-ZX-Fury-ZX-Pump.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568400",
      "smallImageUrl": "https://images.stockx.com/images/adidas-ZX-Fury-ZX-Pump.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568400",
      "thumbUrl": "https://images.stockx.com/images/adidas-ZX-Fury-ZX-Pump.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568400"
    }
  },
  {
    "id": "c78ed6f3-15c4-4012-bb92-365906ddb6fc",
    "brand": "adidas",
    "colorway": "Black/Black-Green",
    "gender": "men",
    "name": "Black Green",
    "releaseDate": "2021-03-19",
    "retailPrice": 180,
    "shoe": "adidas Futurecraft 4D 2021",
    "styleId": "FZ2560",
    "title": "adidas Futurecraft 4D 2021 Black Green",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-Futurecraft-4D-2021-Black-Green.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614964326",
      "smallImageUrl": "https://images.stockx.com/images/adidas-Futurecraft-4D-2021-Black-Green.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614964326",
      "thumbUrl": "https://images.stockx.com/images/adidas-Futurecraft-4D-2021-Black-Green.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614964326"
    }
  },
  {
    "id": "ef3ec874-9073-4a6e-84b1-ddca8be6fb31",
    "brand": "Nike",
    "colorway": "Speed Yellow/Shimmer-White-Siren Red-Black-Pure Platinum",
    "gender": "men",
    "name": "Fried Chicken",
    "releaseDate": "2021-03-19",
    "retailPrice": 140,
    "shoe": "Nike Air Max 90",
    "styleId": "DD5481-735",
    "title": "Nike Air Max 90 Fried Chicken",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Air-Max-90-Fried-Chicken.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614290857",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Air-Max-90-Fried-Chicken.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614290857",
      "thumbUrl": "https://images.stockx.com/images/Nike-Air-Max-90-Fried-Chicken.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614290857"
    }
  },
  {
    "id": "f7c0619a-5362-4be8-bb8e-9b3b22ad15fd",
    "brand": "adidas",
    "colorway": "Sea Teal/Sea Teal/Sea Teal",
    "gender": "men",
    "name": "Sea Teal",
    "releaseDate": "2021-03-19",
    "retailPrice": 250,
    "shoe": "adidas Yeezy QNTM",
    "styleId": "GY7926",
    "title": "adidas Yeezy QNTM Sea Teal",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-Yeezy-QNTM-Sea-Teal.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793282",
      "smallImageUrl": "https://images.stockx.com/images/adidas-Yeezy-QNTM-Sea-Teal.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793282",
      "thumbUrl": "https://images.stockx.com/images/adidas-Yeezy-QNTM-Sea-Teal.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793282"
    }
  },
  {
    "id": "33db395f-4c1c-4a07-b24c-5bfac33c51c5",
    "brand": "adidas",
    "colorway": "Navy/Light Blue",
    "gender": "men",
    "name": "Blue",
    "releaseDate": "2021-03-18",
    "retailPrice": 0,
    "shoe": "adidas City Series Valencia",
    "styleId": "FX5631",
    "title": "adidas City Series Valencia Blue",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-City-Series-Valencia-Blue.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399966",
      "smallImageUrl": "https://images.stockx.com/images/adidas-City-Series-Valencia-Blue.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399966",
      "thumbUrl": "https://images.stockx.com/images/adidas-City-Series-Valencia-Blue.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399966"
    }
  },
  {
    "id": "bc36ccd9-de6c-494b-adf9-56e2f5e872e5",
    "brand": "Nike",
    "colorway": "College Navy/Grey",
    "gender": "women",
    "name": "College Navy Grey (W)",
    "releaseDate": "2021-03-18",
    "retailPrice": 100,
    "shoe": "Nike Dunk Low",
    "styleId": "DD1768-400",
    "title": "Nike Dunk Low College Navy Grey (W)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Dunk-Low-College-Navy-Grey-W.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614142833",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Dunk-Low-College-Navy-Grey-W.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614142833",
      "thumbUrl": "https://images.stockx.com/images/Nike-Dunk-Low-College-Navy-Grey-W.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614142833"
    }
  },
  {
    "id": "e7123bce-b738-4270-a8fa-0c6cfc2e4330",
    "brand": "New Balance",
    "colorway": "Beige/Black",
    "gender": "men",
    "name": "Beige Black",
    "releaseDate": "2021-03-17",
    "retailPrice": 85,
    "shoe": "New Balance 237",
    "styleId": "MS237CB",
    "title": "New Balance 237 Beige Black",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/New-Balance-237-Beige-Black.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568372",
      "smallImageUrl": "https://images.stockx.com/images/New-Balance-237-Beige-Black.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568372",
      "thumbUrl": "https://images.stockx.com/images/New-Balance-237-Beige-Black.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568372"
    }
  },
  {
    "id": "02add8eb-4d86-4130-9ce0-fb70618f665c",
    "brand": "adidas",
    "colorway": "Core Black/Cloud White-Core Black",
    "gender": "men",
    "name": "Maite Steenhoudt",
    "releaseDate": "2021-03-16",
    "retailPrice": 80,
    "shoe": "adidas Samba ADV",
    "styleId": "GZ5271",
    "title": "adidas Samba ADV Maite Steenhoudt",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-Samba-ADV-Maite-Steenhoudt.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399972",
      "smallImageUrl": "https://images.stockx.com/images/adidas-Samba-ADV-Maite-Steenhoudt.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399972",
      "thumbUrl": "https://images.stockx.com/images/adidas-Samba-ADV-Maite-Steenhoudt.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399972"
    }
  },
  {
    "id": "dbf4a7c1-61e7-4995-9c7e-7e25ef13bdba",
    "brand": "adidas",
    "colorway": "Cloud White/Soft Vision-Gum",
    "gender": "men",
    "name": "Nora Vasconcellos",
    "releaseDate": "2021-03-16",
    "retailPrice": 100,
    "shoe": "adidas Gazelle ADV",
    "styleId": "H01024",
    "title": "adidas Gazelle ADV Nora Vasconcellos",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-Gazelle-ADV-Nora-Vasconcellos.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399969",
      "smallImageUrl": "https://images.stockx.com/images/adidas-Gazelle-ADV-Nora-Vasconcellos.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399969",
      "thumbUrl": "https://images.stockx.com/images/adidas-Gazelle-ADV-Nora-Vasconcellos.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399969"
    }
  },
  {
    "id": "03527871-a4f4-4c27-8d6a-83cb2a9b41e9",
    "brand": "Nike",
    "colorway": "Multi-Color/Multi-Color",
    "gender": "women",
    "name": "Green Strike (W)",
    "releaseDate": "2021-03-15",
    "retailPrice": 100,
    "shoe": "Nike Dunk Low",
    "styleId": "DD1503-106",
    "title": "Nike Dunk Low Green Strike (W)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Dunk-Low-White-Multicolor-W.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615310431",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Dunk-Low-White-Multicolor-W.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615310431",
      "thumbUrl": "https://images.stockx.com/images/Nike-Dunk-Low-White-Multicolor-W.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615310431"
    }
  },
  {
    "id": "04983846-2088-4c10-9057-e5e4f3bf597d",
    "brand": "adidas",
    "colorway": "Cloud White/Hazy Sky-Core Black",
    "gender": "men",
    "name": "Hazy Sky",
    "releaseDate": "2021-03-15",
    "retailPrice": 90,
    "shoe": "adidas Pro Model",
    "styleId": "GX2534",
    "title": "adidas Pro Model Hazy Sky",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-Pro-Model-Hazy-Sky.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615049639",
      "smallImageUrl": "https://images.stockx.com/images/adidas-Pro-Model-Hazy-Sky.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615049639",
      "thumbUrl": "https://images.stockx.com/images/adidas-Pro-Model-Hazy-Sky.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615049639"
    }
  },
  {
    "id": "2d586f7a-c8d3-4612-8b72-877de5139932",
    "brand": "adidas",
    "colorway": "Cloud White/Collegiate Green-Off White",
    "gender": "men",
    "name": "Primegreen Collegiate Green",
    "releaseDate": "2021-03-15",
    "retailPrice": 100,
    "shoe": "adidas Stan Smith",
    "styleId": "FX5522",
    "title": "adidas Stan Smith Primegreen Collegiate Green",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-Stan-Smith-Primegreen-Collegiate-Green.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399945",
      "smallImageUrl": "https://images.stockx.com/images/adidas-Stan-Smith-Primegreen-Collegiate-Green.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399945",
      "thumbUrl": "https://images.stockx.com/images/adidas-Stan-Smith-Primegreen-Collegiate-Green.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399945"
    }
  },
  {
    "id": "2eb5ab03-ccca-4c72-9517-3a3174494a1c",
    "brand": "Nike",
    "colorway": "Orange Pearl/White",
    "gender": "women",
    "name": "Orange Pearl",
    "releaseDate": "2021-03-15",
    "retailPrice": 100,
    "shoe": "Nike Dunk Low",
    "styleId": "DD1503-102",
    "title": "Nike Dunk Low Orange Pearl",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Dunk-Low-Orange-Pearl-Product.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1613671259",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Dunk-Low-Orange-Pearl-Product.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1613671259",
      "thumbUrl": "https://images.stockx.com/images/Nike-Dunk-Low-Orange-Pearl-Product.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1613671259"
    }
  },
  {
    "id": "85b53991-8b1b-4093-9b00-c44ef961be7e",
    "brand": "adidas",
    "colorway": "Covellite/Covellite/Covellite",
    "gender": "men",
    "name": "Covellite",
    "releaseDate": "2021-03-15",
    "retailPrice": 230,
    "shoe": "adidas Yeezy Boost 380",
    "styleId": "GZ0454",
    "title": "adidas Yeezy Boost 380 Covellite",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-Yeezy-Boost-380-Covellite.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614853562",
      "smallImageUrl": "https://images.stockx.com/images/adidas-Yeezy-Boost-380-Covellite.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614853562",
      "thumbUrl": "https://images.stockx.com/images/adidas-Yeezy-Boost-380-Covellite.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614853562"
    }
  },
  {
    "id": "913b999c-fca6-49aa-89c5-207c30b3c22b",
    "brand": "adidas",
    "colorway": "Cloud White/Collegiate Navy-Off White",
    "gender": "men",
    "name": "Collegiate Navy",
    "releaseDate": "2021-03-15",
    "retailPrice": 100,
    "shoe": "adidas Stan Smith",
    "styleId": "FX5521",
    "title": "adidas Stan Smith Collegiate Navy",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-Stan-Smith-Collegiate-Navy.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568425",
      "smallImageUrl": "https://images.stockx.com/images/adidas-Stan-Smith-Collegiate-Navy.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568425",
      "thumbUrl": "https://images.stockx.com/images/adidas-Stan-Smith-Collegiate-Navy.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568425"
    }
  },
  {
    "id": "fb99e245-558a-4e5b-b60d-103e6a581553",
    "brand": "adidas",
    "colorway": "Cloud White/Core Black-True Pink",
    "gender": "women",
    "name": "I Love Dance (W)",
    "releaseDate": "2021-03-14",
    "retailPrice": 95,
    "shoe": "adidas Forum Low",
    "styleId": "FZ3908",
    "title": "adidas Forum Low I Love Dance (W)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-Forum-Low-I-Love-Dance-W.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399943",
      "smallImageUrl": "https://images.stockx.com/images/adidas-Forum-Low-I-Love-Dance-W.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399943",
      "thumbUrl": "https://images.stockx.com/images/adidas-Forum-Low-I-Love-Dance-W.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399943"
    }
  },
  {
    "id": "117a507b-f50a-493b-9ce2-b71b42ccad06",
    "brand": "adidas",
    "colorway": "Cream/Cream/Cream",
    "gender": "men",
    "name": "Cream",
    "releaseDate": "2021-03-13",
    "retailPrice": 240,
    "shoe": "adidas Yeezy Boost 700 V2",
    "styleId": "GY7924",
    "title": "adidas Yeezy Boost 700 V2 Cream",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-Yeezy-700-V2-Cream.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614705950",
      "smallImageUrl": "https://images.stockx.com/images/adidas-Yeezy-700-V2-Cream.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614705950",
      "thumbUrl": "https://images.stockx.com/images/adidas-Yeezy-700-V2-Cream.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614705950"
    }
  },
  {
    "id": "2a42b781-5476-466b-a647-d610c74dbe9b",
    "brand": "Vans",
    "colorway": "White/Black",
    "gender": "men",
    "name": "Skull Bandanna White Black",
    "releaseDate": "2021-03-13",
    "retailPrice": 55,
    "shoe": "Vans Slip-On",
    "styleId": "",
    "title": "Vans Slip-On Skull Bandanna White Black",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Vans-Slip-On-Skull-Bandanna-White-Black.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399940",
      "smallImageUrl": "https://images.stockx.com/images/Vans-Slip-On-Skull-Bandanna-White-Black.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399940",
      "thumbUrl": "https://images.stockx.com/images/Vans-Slip-On-Skull-Bandanna-White-Black.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399940"
    }
  },
  {
    "id": "3050e6b8-d806-4959-beff-fbbe3e0a5018",
    "brand": "Jordan",
    "colorway": "White/Multi-Color-White",
    "gender": "women",
    "name": "Change The World (W)",
    "releaseDate": "2021-03-13",
    "retailPrice": 190,
    "shoe": "Jordan 9 Retro",
    "styleId": "CV0420-100",
    "title": "Jordan 9 Retro Change The World (W)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Air-Jordan-9-Retro-Change-The-World-W.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614719581",
      "smallImageUrl": "https://images.stockx.com/images/Air-Jordan-9-Retro-Change-The-World-W.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614719581",
      "thumbUrl": "https://images.stockx.com/images/Air-Jordan-9-Retro-Change-The-World-W.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614719581"
    }
  },
  {
    "id": "9a1afbb2-1564-493b-9b5a-6700e0efbc4f",
    "brand": "Reebok",
    "colorway": "Sandtrap/Chalk",
    "gender": "men",
    "name": "Awake NY Snakeskin",
    "releaseDate": "2021-03-13",
    "retailPrice": 120,
    "shoe": "Reebok Classic Leather",
    "styleId": "H03327",
    "title": "Reebok Classic Leather Awake NY Snakeskin",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Reebok-Classic-Leather-Awake-NY-Snakeskin-1.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615501644",
      "smallImageUrl": "https://images.stockx.com/images/Reebok-Classic-Leather-Awake-NY-Snakeskin-1.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615501644",
      "thumbUrl": "https://images.stockx.com/images/Reebok-Classic-Leather-Awake-NY-Snakeskin-1.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615501644"
    }
  },
  {
    "id": "a63fe342-bd82-4765-b49d-875d0538e918",
    "brand": "New Balance",
    "colorway": "Black/Navy/Burgundy-Blue",
    "gender": "men",
    "name": "Black Navy Burgundy",
    "releaseDate": "2021-03-13",
    "retailPrice": 120,
    "shoe": "New Balance 57/40",
    "styleId": "",
    "title": "New Balance 57/40 Black Navy Burgundy",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/New-Balance-57-40-Black-Navy-Burgundy.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568377",
      "smallImageUrl": "https://images.stockx.com/images/New-Balance-57-40-Black-Navy-Burgundy.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568377",
      "thumbUrl": "https://images.stockx.com/images/New-Balance-57-40-Black-Navy-Burgundy.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568377"
    }
  },
  {
    "id": "a7f64c98-e1c9-466c-883b-70d34bba6ae7",
    "brand": "Vans",
    "colorway": "White/Black",
    "gender": "men",
    "name": "Skull Bandanna White Black",
    "releaseDate": "2021-03-13",
    "retailPrice": 70,
    "shoe": "Vans Style 36",
    "styleId": "",
    "title": "Vans Style 36 Skull Bandanna White Black",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Vans-Style-36-Skull-Bandanna-White-Black.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399964",
      "smallImageUrl": "https://images.stockx.com/images/Vans-Style-36-Skull-Bandanna-White-Black.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399964",
      "thumbUrl": "https://images.stockx.com/images/Vans-Style-36-Skull-Bandanna-White-Black.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399964"
    }
  },
  {
    "id": "abe299f7-5cf7-4d48-ac88-1dd5ca28b4cf",
    "brand": "Reebok",
    "colorway": "Sandtrap/Chalk",
    "gender": "men",
    "name": "Awake NY Snakeskin",
    "releaseDate": "2021-03-13",
    "retailPrice": 120,
    "shoe": "Reebok Club C",
    "styleId": "H03328",
    "title": "Reebok Club C Awake NY Snakeskin",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Reebok-Club-C-Awake-NY-Snakeskin-1.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615501609",
      "smallImageUrl": "https://images.stockx.com/images/Reebok-Club-C-Awake-NY-Snakeskin-1.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615501609",
      "thumbUrl": "https://images.stockx.com/images/Reebok-Club-C-Awake-NY-Snakeskin-1.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615501609"
    }
  },
  {
    "id": "3b9b2c81-4088-4e42-bb98-7eb944664a8b",
    "brand": "Converse",
    "colorway": "White/Orange-Red",
    "gender": "men",
    "name": "Spacesuit NASA White Orange",
    "releaseDate": "2021-03-12",
    "retailPrice": 110,
    "shoe": "Converse Chuck Taylor All-Star 100 Hi",
    "styleId": "",
    "title": "Converse Chuck Taylor All-Star 100 Hi Spacesuit NASA White Orange",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Converse-Chuck-Taylor-All-Star-100-Hi-Spacesuit-NASA-White-Orange.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568367",
      "smallImageUrl": "https://images.stockx.com/images/Converse-Chuck-Taylor-All-Star-100-Hi-Spacesuit-NASA-White-Orange.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568367",
      "thumbUrl": "https://images.stockx.com/images/Converse-Chuck-Taylor-All-Star-100-Hi-Spacesuit-NASA-White-Orange.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568367"
    }
  },
  {
    "id": "581bf849-fe85-417c-bdee-c5236c7e11c1",
    "brand": "Nike",
    "colorway": "White/Neo Teal",
    "gender": "men",
    "name": "White Neo Teal",
    "releaseDate": "2021-03-12",
    "retailPrice": 160,
    "shoe": "Nike Air Structure OG",
    "styleId": "CV3492-100",
    "title": "Nike Air Structure OG White Neo Teal",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Air-Structure-OG-White-Neo-Teal.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399927",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Air-Structure-OG-White-Neo-Teal.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399927",
      "thumbUrl": "https://images.stockx.com/images/Nike-Air-Structure-OG-White-Neo-Teal.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615399927"
    }
  },
  {
    "id": "7c0884b3-0e60-46f0-9488-a5692f6dc645",
    "brand": "Nike",
    "colorway": "Blue/White-Gum",
    "gender": "men",
    "name": "Carpet Company",
    "releaseDate": "2021-03-12",
    "retailPrice": 120,
    "shoe": "Nike SB Dunk High",
    "styleId": "CV1677-100",
    "title": "Nike SB Dunk High Carpet Company",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-SB-Dunk-High-Carpet-Company-Product.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615578910",
      "smallImageUrl": "https://images.stockx.com/images/Nike-SB-Dunk-High-Carpet-Company-Product.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615578910",
      "thumbUrl": "https://images.stockx.com/images/Nike-SB-Dunk-High-Carpet-Company-Product.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615578910"
    }
  },
  {
    "id": "c0f3fc02-4632-4230-a944-124d12ca6fac",
    "brand": "Nike",
    "colorway": "Particle Grey/Chambray Blue",
    "gender": "men",
    "name": "Space Hippie",
    "releaseDate": "2021-03-12",
    "retailPrice": 150,
    "shoe": "Nike Cosmic Unity",
    "styleId": "DA6725-002/DD2737-002",
    "title": "Nike Cosmic Unity Space Hippie",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Cosmic-Unity-Space-Hippie.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1613645235",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Cosmic-Unity-Space-Hippie.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1613645235",
      "thumbUrl": "https://images.stockx.com/images/Nike-Cosmic-Unity-Space-Hippie.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1613645235"
    }
  },
  {
    "id": "d58c77ee-200e-4720-a640-03e9fe93d2f1",
    "brand": "Converse",
    "colorway": "White/Blue-Red",
    "gender": "men",
    "name": "Spacesuit NASA White Blue",
    "releaseDate": "2021-03-12",
    "retailPrice": 110,
    "shoe": "Converse Chuck Taylor All-Star 100 Hi",
    "styleId": "",
    "title": "Converse Chuck Taylor All-Star 100 Hi Spacesuit NASA White Blue",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Converse-Chuck-Taylor-All-Star-100-Hi-Spacesuit-NASA-White-Blue.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568380",
      "smallImageUrl": "https://images.stockx.com/images/Converse-Chuck-Taylor-All-Star-100-Hi-Spacesuit-NASA-White-Blue.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568380",
      "thumbUrl": "https://images.stockx.com/images/Converse-Chuck-Taylor-All-Star-100-Hi-Spacesuit-NASA-White-Blue.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568380"
    }
  },
  {
    "id": "ef6d55ad-1f8b-4db6-ab51-269c68c9ea0e",
    "brand": "Nike",
    "colorway": "White/Green-Gold",
    "gender": "men",
    "name": "St Patricks Day (2021)",
    "releaseDate": "2021-03-12",
    "retailPrice": 140,
    "shoe": "Nike Air Max 90",
    "styleId": "DD8555-300",
    "title": "Nike Air Max 90 St Patricks Day (2021)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Air-Max-90-St-Patricks-Day-2021.png?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1611008700",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Air-Max-90-St-Patricks-Day-2021.png?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1611008700",
      "thumbUrl": "https://images.stockx.com/images/Nike-Air-Max-90-St-Patricks-Day-2021.png?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1611008700"
    }
  },
  {
    "id": "0bfb1c42-5442-485f-8300-9c360503601a",
    "brand": "Converse",
    "colorway": "Twill/Light Grey-Egret",
    "gender": "men",
    "name": "Textile Patchwork Pack Twill",
    "releaseDate": "2021-03-11",
    "retailPrice": 100,
    "shoe": "Converse Chuck Taylor All-Star 70 Hi",
    "styleId": "",
    "title": "Converse Chuck Taylor All-Star 70 Hi Textile Patchwork Pack Twill",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Converse-Chuck-Taylor-All-Star-70-Hi-Textile-Patchwork-Pack-Twill.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568392",
      "smallImageUrl": "https://images.stockx.com/images/Converse-Chuck-Taylor-All-Star-70-Hi-Textile-Patchwork-Pack-Twill.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568392",
      "thumbUrl": "https://images.stockx.com/images/Converse-Chuck-Taylor-All-Star-70-Hi-Textile-Patchwork-Pack-Twill.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568392"
    }
  },
  {
    "id": "0cb0e31f-a9bc-498f-8085-a2551d29cfaf",
    "brand": "Nike",
    "colorway": "Sail/Black-White",
    "gender": "men",
    "name": "Undercover Sail",
    "releaseDate": "2021-03-11",
    "retailPrice": 160,
    "shoe": "Nike Overbreak SP",
    "styleId": "DD1789-200",
    "title": "Nike Overbreak SP Undercover Sail",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Overbreak-SP-Undercover-Sail.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612514243",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Overbreak-SP-Undercover-Sail.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612514243",
      "thumbUrl": "https://images.stockx.com/images/Nike-Overbreak-SP-Undercover-Sail.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612514243"
    }
  },
  {
    "id": "5916eb9f-3c61-4fc2-aac5-84e924e9d581",
    "brand": "Nike",
    "colorway": "Black/White/Sail",
    "gender": "men",
    "name": "Undercover Black",
    "releaseDate": "2021-03-11",
    "retailPrice": 160,
    "shoe": "Nike Overbreak SP",
    "styleId": "DD1789-001",
    "title": "Nike Overbreak SP Undercover Black",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Overbreak-SP-Undercover-Black.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612514213",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Overbreak-SP-Undercover-Black.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612514213",
      "thumbUrl": "https://images.stockx.com/images/Nike-Overbreak-SP-Undercover-Black.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612514213"
    }
  },
  {
    "id": "7c3543e4-2cb3-44fb-a41b-bfde8c9678fa",
    "brand": "Converse",
    "colorway": "Peach/Sheer Lilac/Egret",
    "gender": "men",
    "name": "Textile Patchwork Pack Peach",
    "releaseDate": "2021-03-11",
    "retailPrice": 100,
    "shoe": "Converse Chuck Taylor All-Star 70 Hi",
    "styleId": "",
    "title": "Converse Chuck Taylor All-Star 70 Hi Textile Patchwork Pack Peach",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Converse-Chuck-Taylor-All-Star-70-Hi-Textile-Patchwork-Pack-Peach.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568386",
      "smallImageUrl": "https://images.stockx.com/images/Converse-Chuck-Taylor-All-Star-70-Hi-Textile-Patchwork-Pack-Peach.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568386",
      "thumbUrl": "https://images.stockx.com/images/Converse-Chuck-Taylor-All-Star-70-Hi-Textile-Patchwork-Pack-Peach.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568386"
    }
  },
  {
    "id": "b3630ec2-b2c0-456e-84a0-edf9204a9383",
    "brand": "Nike",
    "colorway": "Black/Black-Varsity Red",
    "gender": "men",
    "name": "Black Red",
    "releaseDate": "2021-03-11",
    "retailPrice": 160,
    "shoe": "Nike Lebron 18 Low",
    "styleId": "CV7562-001",
    "title": "Nike Lebron 18 Low Black Red",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Lebron-18-Low-Black-Red.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614290887",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Lebron-18-Low-Black-Red.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614290887",
      "thumbUrl": "https://images.stockx.com/images/Nike-Lebron-18-Low-Black-Red.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614290887"
    }
  },
  {
    "id": "00230645-15ec-4da9-93d7-2f3d64d39f85",
    "brand": "Nike",
    "colorway": "White/Black-White",
    "gender": "child",
    "name": "White Black (PS)",
    "releaseDate": "2021-03-10",
    "retailPrice": 65,
    "shoe": "Nike Dunk Low Retro",
    "styleId": "CW1588-100",
    "title": "Nike Dunk Low Retro White Black (PS)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Dunk-Low-Retro-White-Black-PS.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612904678",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Dunk-Low-Retro-White-Black-PS.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612904678",
      "thumbUrl": "https://images.stockx.com/images/Nike-Dunk-Low-Retro-White-Black-PS.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612904678"
    }
  },
  {
    "id": "124cef0b-081a-44e5-aec4-bccc7036838b",
    "brand": "Nike",
    "colorway": "Wolf Grey/Lime",
    "gender": "men",
    "name": "Wolf Grey",
    "releaseDate": "2021-03-10",
    "retailPrice": 140,
    "shoe": "Nike Space Hippie 01",
    "styleId": "",
    "title": "Nike Space Hippie 01 Wolf Grey",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Space-Hippie-01-Wolf-Grey.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793249",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Space-Hippie-01-Wolf-Grey.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793249",
      "thumbUrl": "https://images.stockx.com/images/Nike-Space-Hippie-01-Wolf-Grey.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793249"
    }
  },
  {
    "id": "206664d5-841d-4791-b415-8a66b83db48d",
    "brand": "Nike",
    "colorway": "White/Orange Blaze",
    "gender": "women",
    "name": "Syracuse (W) (2021)",
    "releaseDate": "2021-03-10",
    "retailPrice": 110,
    "shoe": "Nike Dunk High",
    "styleId": "DD1869-100",
    "title": "Nike Dunk High Syracuse (W) (2021)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Dunk-High-Syracuse-W-2021.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614461623",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Dunk-High-Syracuse-W-2021.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614461623",
      "thumbUrl": "https://images.stockx.com/images/Nike-Dunk-High-Syracuse-W-2021.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614461623"
    }
  },
  {
    "id": "59f9f4de-a8b8-46fc-841f-42961d3567e7",
    "brand": "Nike",
    "colorway": "White/Black-White",
    "gender": "child",
    "name": "White Black (GS)",
    "releaseDate": "2021-03-10",
    "retailPrice": 85,
    "shoe": "Nike Dunk Low Retro",
    "styleId": "CW1590-100",
    "title": "Nike Dunk Low Retro White Black (GS)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Dunk-Low-Retro-White-Black-GS-Product.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1613360456",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Dunk-Low-Retro-White-Black-GS-Product.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1613360456",
      "thumbUrl": "https://images.stockx.com/images/Nike-Dunk-Low-Retro-White-Black-GS-Product.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1613360456"
    }
  },
  {
    "id": "5e6a1e57-1c7d-435a-82bd-5666a13560fe",
    "brand": "Nike",
    "colorway": "White/Black",
    "gender": "men",
    "name": "Retro White Black (2021)",
    "releaseDate": "2021-03-10",
    "retailPrice": 110,
    "shoe": "Nike Dunk Low",
    "styleId": "DD1391-100",
    "title": "Nike Dunk Low Retro White Black (2021)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Dunk-Low-Retro-White-Black-2021-Product.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1611084516",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Dunk-Low-Retro-White-Black-2021-Product.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1611084516",
      "thumbUrl": "https://images.stockx.com/images/Nike-Dunk-Low-Retro-White-Black-2021-Product.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1611084516"
    }
  },
  {
    "id": "63270715-ad06-4480-a6a2-890bc421c04c",
    "brand": "Nike",
    "colorway": "White/Black-White",
    "gender": "toddler",
    "name": "White Black (TD)",
    "releaseDate": "2021-03-10",
    "retailPrice": 50,
    "shoe": "Nike Dunk Low Retro",
    "styleId": "CW1589-100",
    "title": "Nike Dunk Low Retro White Black (TD)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Dunk-Low-Retro-White-Black-TD.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615488172",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Dunk-Low-Retro-White-Black-TD.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615488172",
      "thumbUrl": "https://images.stockx.com/images/Nike-Dunk-Low-Retro-White-Black-TD.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615488172"
    }
  },
  {
    "id": "971a1f88-ed5f-4f60-9202-a454b4bc7772",
    "brand": "Nike",
    "colorway": "White/Orange",
    "gender": "child",
    "name": "Syracuse (GS) (2021)",
    "releaseDate": "2021-03-10",
    "retailPrice": 90,
    "shoe": "Nike Dunk High SP",
    "styleId": "DB2179-100",
    "title": "Nike Dunk High SP Syracuse (GS) (2021)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Dunk-High-SP-Syracuse-GS-2021.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568411",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Dunk-High-SP-Syracuse-GS-2021.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568411",
      "thumbUrl": "https://images.stockx.com/images/Nike-Dunk-High-SP-Syracuse-GS-2021.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568411"
    }
  },
  {
    "id": "bf9de8ac-4056-4134-a89a-3b59f8421522",
    "brand": "Nike",
    "colorway": "White/Orange Blaze",
    "gender": "men",
    "name": "Syracuse (2021)",
    "releaseDate": "2021-03-10",
    "retailPrice": 110,
    "shoe": "Nike Dunk High",
    "styleId": "DD1399-101",
    "title": "Nike Dunk High Syracuse (2021)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Dunk-High-Syracuse-2021.png?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1610190459",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Dunk-High-Syracuse-2021.png?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1610190459",
      "thumbUrl": "https://images.stockx.com/images/Nike-Dunk-High-Syracuse-2021.png?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1610190459"
    }
  },
  {
    "id": "e175c189-cf87-4007-bc94-e5b919c4c75c",
    "brand": "Nike",
    "colorway": "White/Black",
    "gender": "women",
    "name": "White Black (2021) (W)",
    "releaseDate": "2021-03-10",
    "retailPrice": 100,
    "shoe": "Nike Dunk Low",
    "styleId": "DD1503-101",
    "title": "Nike Dunk Low White Black (2021) (W)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Dunk-Low-White-Black-2021-W-Product.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1611766850",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Dunk-Low-White-Black-2021-W-Product.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1611766850",
      "thumbUrl": "https://images.stockx.com/images/Nike-Dunk-Low-White-Black-2021-W-Product.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1611766850"
    }
  },
  {
    "id": "dfb85e45-59b7-45ff-a8cd-a8ae81b3d537",
    "brand": "Nike",
    "colorway": "Barely Green/Black",
    "gender": "men",
    "name": "SE All-Star (2021)",
    "releaseDate": "2021-03-09",
    "retailPrice": 110,
    "shoe": "Nike Dunk High",
    "styleId": "DD1398-300",
    "title": "Nike Dunk High SE All-Star (2021)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Dunk-High-SE-All-Star-Product.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615579078",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Dunk-High-SE-All-Star-Product.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615579078",
      "thumbUrl": "https://images.stockx.com/images/Nike-Dunk-High-SE-All-Star-Product.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615579078"
    }
  },
  {
    "id": "e81f8edd-53d5-4b56-8d1f-f47ed161d845",
    "brand": "adidas",
    "colorway": "Footwear White/Core Black-Royal Blue",
    "gender": "men",
    "name": "Royal Blue",
    "releaseDate": "2021-03-08",
    "retailPrice": 100,
    "shoe": "adidas Forum Mid",
    "styleId": "FY6796",
    "title": "adidas Forum Mid Royal Blue",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-Forum-Mid-Royal-Blue.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568422",
      "smallImageUrl": "https://images.stockx.com/images/adidas-Forum-Mid-Royal-Blue.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568422",
      "thumbUrl": "https://images.stockx.com/images/adidas-Forum-Mid-Royal-Blue.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615568422"
    }
  },
  {
    "id": "9010c398-1358-4ee0-9991-323a1fed47e3",
    "brand": "Nike",
    "colorway": "Blue/Teal/Light Blue",
    "gender": "men",
    "name": "Amalgam",
    "releaseDate": "2021-03-07",
    "retailPrice": 150,
    "shoe": "Nike Cosmic Unity",
    "styleId": "DA6725-500",
    "title": "Nike Cosmic Unity Amalgam",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Nike-Cosmic-Unity-Amalgam.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1613645232",
      "smallImageUrl": "https://images.stockx.com/images/Nike-Cosmic-Unity-Amalgam.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1613645232",
      "thumbUrl": "https://images.stockx.com/images/Nike-Cosmic-Unity-Amalgam.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1613645232"
    }
  },
  {
    "id": "1c6d7f01-6e6b-4c96-96d9-892273f2d763",
    "brand": "Jordan",
    "colorway": "White/University Blue-Black",
    "gender": "child",
    "name": "White University Blue Black (GS)",
    "releaseDate": "2021-03-06",
    "retailPrice": 130,
    "shoe": "Jordan 1 Retro High",
    "styleId": "575441-134",
    "title": "Jordan 1 Retro High White University Blue Black (GS)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Air-Jordan-1-Retro-High-White-University-Blue-Black-GS-Product.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612902623",
      "smallImageUrl": "https://images.stockx.com/images/Air-Jordan-1-Retro-High-White-University-Blue-Black-GS-Product.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612902623",
      "thumbUrl": "https://images.stockx.com/images/Air-Jordan-1-Retro-High-White-University-Blue-Black-GS-Product.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1612902623"
    }
  },
  {
    "id": "3d2b0362-4ae2-4983-bd5d-df0dc40e5183",
    "brand": "Jordan",
    "colorway": "University Blue/White-Black-University Blue",
    "gender": "toddler",
    "name": "University Blue (TD)",
    "releaseDate": "2021-03-06",
    "retailPrice": 0,
    "shoe": "Jordan 1 Retro High",
    "styleId": "AQ2665-134",
    "title": "Jordan 1 Retro High University Blue (TD)",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Air-Jordan-1-Retro-High-University-Blue-TD.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615067635",
      "smallImageUrl": "https://images.stockx.com/images/Air-Jordan-1-Retro-High-University-Blue-TD.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615067635",
      "thumbUrl": "https://images.stockx.com/images/Air-Jordan-1-Retro-High-University-Blue-TD.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615067635"
    }
  },
  {
    "id": "4cb94927-7617-449d-85e1-19202d160bd9",
    "brand": "Reebok",
    "colorway": "White/Black",
    "gender": "men",
    "name": "Why Not Us",
    "releaseDate": "2021-03-06",
    "retailPrice": 140,
    "shoe": "Reebok Question Mid",
    "styleId": "GX5260",
    "title": "Reebok Question Mid Why Not Us",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Reebok-Question-Mid-Why-Not-Us.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793252",
      "smallImageUrl": "https://images.stockx.com/images/Reebok-Question-Mid-Why-Not-Us.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793252",
      "thumbUrl": "https://images.stockx.com/images/Reebok-Question-Mid-Why-Not-Us.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614793252"
    }
  },
  {
    "id": "51833186-9af8-4ced-9cd5-46798fe71a2d",
    "brand": "adidas",
    "colorway": "Cloud White/Cloud White",
    "gender": "men",
    "name": "Cloud White",
    "releaseDate": "2021-03-06",
    "retailPrice": 200,
    "shoe": "adidas Yeezy 450",
    "styleId": "H68038",
    "title": "adidas Yeezy 450 Cloud White",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-Yeezy-450-Cloud-White-Product.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615564111",
      "smallImageUrl": "https://images.stockx.com/images/adidas-Yeezy-450-Cloud-White-Product.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615564111",
      "thumbUrl": "https://images.stockx.com/images/adidas-Yeezy-450-Cloud-White-Product.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615564111"
    }
  },
  {
    "id": "7ddc84c2-033a-4bc1-ba53-270d0b078faf",
    "brand": "adidas",
    "colorway": "White/Multicolor",
    "gender": "men",
    "name": "atmos Mt. Fuji",
    "releaseDate": "2021-03-06",
    "retailPrice": 140,
    "shoe": "adidas Superstar",
    "styleId": "GX7791",
    "title": "adidas Superstar atmos Mt. Fuji",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/adidas-Superstar-atmos-Mt-Fuji.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615310452",
      "smallImageUrl": "https://images.stockx.com/images/adidas-Superstar-atmos-Mt-Fuji.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615310452",
      "thumbUrl": "https://images.stockx.com/images/adidas-Superstar-atmos-Mt-Fuji.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1615310452"
    }
  },
  {
    "id": "80638461-7324-4bf1-ae70-3d9e0d5b1be4",
    "brand": "Reebok",
    "colorway": "Wild Brown/Maroon/Matte Gold",
    "gender": "men",
    "name": "SNS Walking",
    "releaseDate": "2021-03-06",
    "retailPrice": 119,
    "shoe": "Reebok Classic Leather Legacy",
    "styleId": "GZ8707",
    "title": "Reebok Classic Leather Legacy SNS Walking",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Reebok-Classic-Leather-Legacy-SNS-Walking.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614964307",
      "smallImageUrl": "https://images.stockx.com/images/Reebok-Classic-Leather-Legacy-SNS-Walking.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614964307",
      "thumbUrl": "https://images.stockx.com/images/Reebok-Classic-Leather-Legacy-SNS-Walking.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614964307"
    }
  },
  {
    "id": "d89d6c7b-3605-44d0-973f-39ed17037189",
    "brand": "Reebok",
    "colorway": "Stucco/Wild Brown/Modern Beige",
    "gender": "men",
    "name": "SNS Walking",
    "releaseDate": "2021-03-06",
    "retailPrice": 119,
    "shoe": "Reebok Classic Leather",
    "styleId": "GZ8706",
    "title": "Reebok Classic Leather SNS Walking",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Reebok-Classic-Leather-SNS-Walking.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614964309",
      "smallImageUrl": "https://images.stockx.com/images/Reebok-Classic-Leather-SNS-Walking.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614964309",
      "thumbUrl": "https://images.stockx.com/images/Reebok-Classic-Leather-SNS-Walking.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1614964309"
    }
  },
  {
    "id": "dd830775-6cb7-40f4-8ef3-4d944c897fce",
    "brand": "Jordan",
    "colorway": "White/University Blue-Black",
    "gender": "men",
    "name": "White University Blue Black",
    "releaseDate": "2021-03-06",
    "retailPrice": 170,
    "shoe": "Jordan 1 Retro High",
    "styleId": "555088-134",
    "title": "Jordan 1 Retro High White University Blue Black",
    "year": 2021,
    "media": {
      "imageUrl": "https://images.stockx.com/images/Air-Jordan-1-Retro-High-White-University-Blue-Black-Product.jpg?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1611777406",
      "smallImageUrl": "https://images.stockx.com/images/Air-Jordan-1-Retro-High-White-University-Blue-Black-Product.jpg?fit=fill&bg=FFFFFF&w=300&h=214&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1611777406",
      "thumbUrl": "https://images.stockx.com/images/Air-Jordan-1-Retro-High-White-University-Blue-Black-Product.jpg?fit=fill&bg=FFFFFF&w=140&h=100&auto=format,compress&trim=color&q=90&dpr=2&updated_at=1611777406"
    }
  }
]